<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold blooded : " . $sheep->cold_blooded . "<br><br>";

$frog = new frog("buduk");

echo "Nama Kodok : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "Cold blooded : $frog->cold_blooded <br>";
$frog->jump();


$ape = new ape("Kera Sakti");

echo "<br><br>Name : " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold blooded : " . $ape->cold_blooded . "<br>";
$ape->yell();
